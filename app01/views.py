import base64
import mimetypes
import time

from django.http import JsonResponse
from rest_framework import viewsets
from .models import Info
from .serializers import InfoSerializer
from .page import MyPageNumberPagination
from apscheduler.schedulers.background import BackgroundScheduler
from django_apscheduler.jobstores import DjangoJobStore, register_events, register_job
from apscheduler.triggers.cron import CronTrigger
from datetime import date, timedelta
import requests, json
from openpyxl import Workbook


class InfoView(viewsets.ModelViewSet):
    queryset = Info.objects.filter(delete=False).order_by('-create_time')
    serializer_class = InfoSerializer
    pagination_class = MyPageNumberPagination

    def destroy(self, request, *args, **kwargs):
        rid = kwargs.get('pk')
        Info.objects.filter(id=rid).update(delete=1)
        return JsonResponse({"success": True, "deleted_id": rid})


try:
   # cron_trigger = CronTrigger(day_of_week='mon-sun', hour=16, minute=27, second=50)

    cron_trigger = CronTrigger(day_of_week='mon-sun', hour=9, minute=0, second=0)

    scheduler = BackgroundScheduler(timezone='Asia/Shanghai')
    scheduler.add_jobstore(DjangoJobStore(), 'default')

    CORP_ID = 'ww89045d315bcf9f7f'
    AGENTID = "1000026"
    SECRET = "652LTrsIcWaNFcJ16GDqKV0koDJRgi7gckvWPEK1vG0"

    class WeChatPub:
        s = requests.session()

        def __init__(self):
            self.token = self.get_token()

        def get_token(self):
            url = f"https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={CORP_ID}&corpsecret={SECRET}"
            rep = self.s.get(url)
            if rep.status_code != 200:
                print("request failed.")
                return
            return json.loads(rep.content)['access_token']

        def send_msg(self, file_name, info_str):
            utf = info_str.encode('utf-8')
            info_bytes = base64.b64encode(utf).decode('utf-8')

            url = "https://qyapi.weixin.qq.com/cgi-bin/exmail/app/compose_send?access_token=" + self.token

            header = {
                "Content-Type": "application/json"
            }
            form_data = {
                "to": {
                   # "emails":["quankaili@ai-yc.com","zihan.wang@ai-yc.com"]    # 测试
                    "emails":["ye.chen@ai-yc.com", "chunfeng.gu@ai-yc.com"]
                },
                "subject": "优层官网留言",
                "content": "以上附件是官网昨日留言信息汇总",
                "attachment_list": [
                    {
                        "file_name": file_name,
                        "content": info_bytes
                    }
                ]
            }
            rep = self.s.post(url, data=json.dumps(form_data), headers=header)

            if rep.status_code != 200:
                print("request failed.")
                return
            print(json.loads(rep.content))
            return json.loads(rep.content)


    @register_job(scheduler, trigger=cron_trigger, id='Web', replace_existing=True)
    def job():
        today = date.today() - timedelta(days=1)

        datas = Info.objects.filter(create_time__date=today, delete=False).all()

        info_str = '姓名\t时间\t电话\t留言\n'

        for i in datas:
            name = i.name if i.name is not None else ""
            time = i.create_time.strftime('%Y-%m-%d %H:%M')
            phone = i.phone if i.phone is not None else ""
            info = i.info
            info_str += f'{name}\t{time}\t{phone}\t{info} \n'

        #  print(info_str)
        file_name = 'YcOfficial.xlsx'

        wechat = WeChatPub()
        if datas:
            wechat.send_msg(file_name, info_str)
            print('已发送...')
        else:
            print('今日无留言...')


    register_events(scheduler)
    scheduler.start()

except Exception as e:
    print(e)
