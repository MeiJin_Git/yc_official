from django.db import models


class Info(models.Model):
    name = models.CharField(verbose_name='名称', max_length=256, null=True)
    phone = models.CharField(verbose_name='电话', max_length=256, null=True)
    info = models.CharField(verbose_name='留言信息', max_length=256, null=True)
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now=True, null=True)
    delete = models.BooleanField(verbose_name="逻辑删除", default=False)
