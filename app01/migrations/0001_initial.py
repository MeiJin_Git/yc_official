# Generated by Django 4.2.2 on 2023-07-03 14:27

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Info",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "name",
                    models.CharField(max_length=256, null=True, verbose_name="名称"),
                ),
                (
                    "phone",
                    models.CharField(max_length=256, null=True, verbose_name="电话"),
                ),
                (
                    "info",
                    models.CharField(max_length=256, null=True, verbose_name="留言信息"),
                ),
                (
                    "create_time",
                    models.DateTimeField(auto_now=True, null=True, verbose_name="创建时间"),
                ),
                ("delete", models.BooleanField(default=False, verbose_name="逻辑删除")),
            ],
        ),
    ]
