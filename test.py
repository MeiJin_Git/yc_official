import requests, json, time

CORP_ID = 'ww89045d315bcf9f7f'
AGENTID = "1000026"
SECRET = "652LTrsIcWaNFcJ16GDqKV0koDJRgi7gckvWPEK1vG0"


class WeChatPub:
    s = requests.session()

    def __init__(self):
        self.token = self.get_token()

    def get_token(self):
        url = f"https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={CORP_ID}&corpsecret={SECRET}"
        rep = self.s.get(url)
        if rep.status_code != 200:
            print("request failed.")
            return
        return json.loads(rep.content)['access_token']

    def send_msg(self, content):
        url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=" + self.token
        header = {
            "Content-Type": "application/json"
        }
        form_data = {
            "touser": "meijin",  # 接收人
            # "toparty": "1",#接收部门
            # "totag": " TagID1 | TagID2 ",#通讯录标签id
            "msgtype": "textcard",
            "agentid": 1000026,  # 应用ID
            "textcard": {
                "title": "官网微信测试",
                "description": content,
                "url": "URL",
                "btntxt": "更多"
            },
            "safe": 0,
            "toemail": "zihan.wang@ai-yc.com"
        }
        rep = self.s.post(url, data=json.dumps(form_data).encode('utf-8'), headers=header)
        if rep.status_code != 200:
            print("request failed.")
            return
        print(json.loads(rep.content))
        print(rep.status_code)
        return json.loads(rep.content)


if __name__ == "__main__":
    wechat = WeChatPub()
    timenow = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    wechat.send_msg(f"<div class=\"gray\">{timenow}</div> <div class=\"normal\">注意！</div><div class=\"highlight\">消息测试</div>")
    print('消息已发送！')
