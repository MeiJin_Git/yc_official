import requests, json, time

CORP_ID = 'ww89045d315bcf9f7f'
AGENTID = "1000026"
SECRET = "652LTrsIcWaNFcJ16GDqKV0koDJRgi7gckvWPEK1vG0"


class WeChatPub:
    s = requests.session()

    def __init__(self):
        self.token = self.get_token()

    def get_token(self):
        url = f"https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={CORP_ID}&corpsecret={SECRET}"
        rep = self.s.get(url)
        if rep.status_code != 200:
            print("request failed.")
            return
        return json.loads(rep.content)['access_token']

    def send_msg(self):
        url = "https://qyapi.weixin.qq.com/cgi-bin/exmail/app/compose_send?access_token=" + self.token


        info = "WydOb25lJywgJzIwMjMtMDctMjYgMTY6NDAnLCAnTm9uZScsICcyMDIzLzA3LzI2L+a1i+ivlTExMTExMSddLApbJ05vbmUnLCAnMjAyMy0wNy0yNiAxNjo1MycsICdOb25lJywgJzIwMjMtMDctMjYv5rWL6K+VMjIyMiddLApbJ+aXoOWQjeensCcsICcyMDIzLTA3LTI2IDE4OjEzJywgJ+aXoOaJi+acuuWPtycsICc3LjI25rWL6Zi/6L6+6L+b6Zi/6YeM55qE6aqE5YKyJ10="

        header = {
            "Content-Type": "application/json"
        }
        form_data = {
             "to": {
               # "emails": ["zihan.wang@ai-yc.com"],
                "emails": ["quankaili@ai-yc.com"],
                #  "userids": ["WangZiHan"]
                # "userids": ["MeiJin"]
                   },
             "subject": "优层官网留言",
             "content": "以上附件是官网昨日留言信息汇总",
             "attachment_list": [
                {
                    "file_name": "YcOfficial.xlsx",
                     "content": info
                }
                ]
            }
        rep = self.s.post(url, data=json.dumps(form_data).encode('utf-8'), headers=header)

        if rep.status_code != 200:
            print("request failed.")
            return
        print(json.loads(rep.content))
        print(rep.status_code)
        return json.loads(rep.content)


if __name__ == "__main__":
    wechat = WeChatPub()
    timenow = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    wechat.send_msg()
